﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{

    private Animator anim;
    private GameObject character;
    private float horizontal;
    private float vertical;
    private float speed;
    public float speedvalue;
    public float runspeedvalue;
    private float slower;
    public int defaultfov;
    public int zoomfov;
    public float zoomtime;
    private GameObject camera;

    void Start()
    {
        camera = GameObject.FindWithTag("MainCamera");
        camera.GetComponent<Camera>().fieldOfView = defaultfov;
        speed = speedvalue;
        slower = speedvalue * 0.75f;
        character = gameObject;
    }
    void Update()
    {
        anim = character.GetComponent<Animator>();
        horizontal = Input.GetAxis("Horizontal");
        vertical = Input.GetAxis("Vertical");

        Animations();
        Controller();
        CameraZoom();
    }
    void Animations()
    {
        anim.SetFloat("vertical", vertical);
        anim.SetFloat("horizontal", horizontal);

        if (Input.GetKeyDown(KeyCode.R))
        {
            anim.SetTrigger("reload");
        }
        if (Input.GetKey(KeyCode.W) && Input.GetKey(KeyCode.LeftShift) ||
         Input.GetKey(KeyCode.S) && Input.GetKey(KeyCode.LeftShift))
        {
            anim.SetBool("run", true);
        }
        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            anim.SetBool("run", false);
        }
    }
    void Controller()
    {
        Vector3 characterMovement = new Vector3(horizontal, 0f, vertical) * speed * Time.deltaTime;
        transform.Translate(characterMovement, Space.Self);

        //vertical movement (walking)
        if (vertical == 1 && horizontal == 1 || vertical == 1 && horizontal == -1 ||
         vertical == -1 && horizontal == 1 || vertical == -1 && horizontal == -1)
        {
            speed = slower;
        }
        else
        {
            speed = speedvalue;
        }
        if (anim.GetBool("run") == true)
        {
            if (vertical == 1 && horizontal == 1 || vertical == 1 && horizontal == -1 ||
                vertical == -1 && horizontal == 1 || vertical == -1 && horizontal == -1)
            {
                speed = runspeedvalue * 0.75f;
            }
            else
            {
                speed = runspeedvalue;

            }
        }
    }
    void CameraZoom()
    {
        if (anim.GetBool("run") == true)
        {
            camera.GetComponent<Camera>().fieldOfView = Mathf.Lerp(camera.GetComponent<Camera>().fieldOfView, zoomfov, Time.deltaTime * zoomtime);
        }
        else
        {
            camera.GetComponent<Camera>().fieldOfView = Mathf.Lerp(camera.GetComponent<Camera>().fieldOfView, defaultfov, Time.deltaTime * zoomtime);
        }
    }
}